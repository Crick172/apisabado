const { Router } = require('express')
const router = Router()

const { renderNoteForm, createNewNote, renderNotes } = require('../controller/notes.controller');

//New note
router.get('/notes/add', renderNoteForm);

router.post('/notes/add', createNewNote);

//Get All Note
router.get('/notes', renderNotes)




module.exports = router