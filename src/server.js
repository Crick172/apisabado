const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const { addListener } = require('process');

// Initializations
const app = express();

// Settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'))
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

//Middelwares
app.use(express.urlencoded({extended: false}));

// Global Variables

// Routes
app.use(require('./routes/index.routes'));
//app.use(require('./routes/notes.routes'));




// Static Files
app.use(express.static(path.join(__dirname, 'public')));


module.exports = app; 